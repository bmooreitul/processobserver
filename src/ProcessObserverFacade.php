<?php

namespace Itul\ProcessObserver;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\Larabuilder\Skeleton\SkeletonClass
 */
class ProcessObserverFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ProcessObserver';
    }

    public static function init(){
        return new \Itul\ProcessObserver\ProcessObserver;
    }

    public static function records($records, $name){
        return self::init()->records($records, $name);
    }
}
