<?php

namespace Itul\ProcessObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Artisan;

class ProcessObserverServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 */
	public function boot(){

		//LOAD MIGRATIONS
		$this->loadMigrationsFrom(__DIR__.'/migrations');

		//LOAD ROUTES
		$this->loadRoutesFrom(__DIR__.'/routes.php');

		//MAKE SURE WE ARE RUNNING IN THE CONSOLE
		if($this->app->runningInConsole()) {			
				
			//DEFINE THE PUBLISHABLE FILES
			$this->publishes([
				__DIR__.'/bootstrap/process-observer.php'     			=> base_path('bootstrap/process-observer.php'),
				__DIR__.'/config/process-observer.php'                	=> config_path('process-observer.php'),
				//__DIR__.'/Models/publishable/QuickbooksQueue.php'       => app_path('Models/QuickbooksQueue.php'),
				//__DIR__.'/Models/publishable/QuickbooksTransaction.php' => app_path('Models/QuickbooksTransaction.php'),
				//__DIR__.'/Controllers/QuickBooksController.php' 		=> app_path('Http/Controllers/QuickBooks/QuickBooksController.php')
			], 'process-observer-assets');

			//AUTO PUBLISH THE ASSETS
			Artisan::call('vendor:publish', ['--tag' => 'process-observer-assets']);

			//RUN MIGRATIONS FOR THIS PACKAGE ONLY AS NEEDED
			foreach(glob(__DIR__.'/migrations/*.php') as $migrationFile) Artisan::call("migrate", ['--path' => str_replace(base_path(), '', $migrationFile)]);
		}
	}
}