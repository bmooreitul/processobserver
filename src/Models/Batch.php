<?php

namespace Itul\ProcessObserver\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
	use HasFactory;

	protected $table = 'process_observer_batches';

	public $watch = false;

	protected $fillable = [
		'name',
		'message',
		'total_records',
		'current_record',
		'pid',
		'average_task_time',
	];

	protected $casts = [
		//....
	];

	//DEFINE ANY MUTATORS THAT NEED TO BE LOADED WITH THE MODEL
	protected $appends = [
		'percentage',
		'duration',
		'eta',
		'running'
	];

	//WHEN THE MODEL HAS BOOTS
	protected static function boot(){

		//CALL THE PARENT BOOT METHOD
		parent::boot();


		//------------ MODEL EVENT LISTENERS -----------		
		
		//DEFAULT BOOT LISTENERS ARE:
		//retrieved,creating,created,updating,updated,saving,saved,deleting,deleted,restoring,restored

		
		static::creating(function($model){            
			$model->pid = getmypid();
		});	

		static::saving(function($model){            
			$now = \Carbon\Carbon::now();
			$seconds = $now->diffInSeconds($model->created_at);
			if(!$seconds || !$model->current_record){

			}
			else{
				try{
					$secondsPerRecord = $seconds/$model->current_record;
				}
				catch(\Exception $e){
					$secondsPerRecord = 15;
				}
				$model->average_task_time = floatval($secondsPerRecord);
			}

			
		});		
		

		//------------ END MODEL EVENT LISTENERS -----------

	}

	public function getRunningAttribute(){
		return shell_exec("ps aux | grep " . $this->pid . " | wc -l") > 0;
	}

	public function getPercentageAttribute(){
		return number_format(($this->current_record/$this->total_records)*100, 2, '.', '');
	}

	public function getDurationAttribute(){
		return [
			'human' 	=> $this->created_at->diffForHumans(['syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE]), 
			'seconds' 	=> $this->created_at->diffInSeconds(\Carbon\Carbon::now())
		];
	}

	public function getEtaAttribute(){

		$now = \Carbon\Carbon::now();
		$seconds = $now->diffInSeconds($this->created_at);
		if(!$seconds || !$this->current_record) return $now->addSeconds(1)->diffForHumans();

		try{
			$secondsPerRecord = $seconds/$this->current_record;
		}
		catch(\Exception $e){
			$secondsPerRecord = 15;
		}

		$recordsRemaining 	= $this->total_records-$this->current_record;
		$estSeconds 		= round($secondsPerRecord*$recordsRemaining);
		return $now->addSeconds($estSeconds)->diffForHumans();
	}	
}
