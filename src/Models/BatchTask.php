<?php

namespace Itul\ProcessObserver\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatchTask extends Model
{
	use HasFactory;

	protected $table = 'process_observer_tasks';

	protected $fillable = [
		'process_observer_batch_id',
		'status',
		'message',
		'message',
		'callback',
	];

	protected $casts = [
		//....
	];
}
