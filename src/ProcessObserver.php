<?php

namespace Itul\ProcessObserver;

class ProcessObserver
{

    private $_records;   

    public static function init(){
        return new \Itul\ProcessObserver\ProcessObserver;
    }    

    public function records($data, $name){

        if(!function_exists('shell_exec')) throw new \Exception("The ProcessObserver package requires the shell_exec function to be enabled");
        if(!function_exists('fastcgi_finish_request')) throw new \Exception("The ProcessObserver package requires the server to run in PHP-FPM");

        //SET THE RECORDS
        $this->_records     = $data;

        //CHECK IF THE PROCESSOR IS ALREADY RUNNING
        if($processor                   = \Itul\ProcessObserver\Models\Batch::where('name', $name)->first()){
            $this->_processor           = $processor;
            $this->_processor->watch    = true;
        }

        //THE PROCESSOR IS NOT RUNNING TO START A NEW ONE
        else{
            $this->_processor = \Itul\ProcessObserver\Models\Batch::create([
                'name'              => $name,
                'message'           => 'Starting',
                'total_records'     => count($this->_records),
                'current_record'    => 0,
            ]);
        }

        return $this;
    }

    public function setMessage($value){
        $this->_processor->update(['message' => $value]);
        return $this;
    }

    public function run($callback){

        if($this->_processor->total_records < 1){
            $this->_processor->delete();
            die();
        }

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        ignore_user_abort(true);
        set_time_limit(0);

        ob_start();
        header("Content-Type: application/json");
        echo json_encode($this->_processor, JSON_PRETTY_PRINT);
        ob_end_flush();
        @ob_flush();
        flush();

        if($this->_processor->watch) die();

        fastcgi_finish_request();

        foreach($this->_records as $k => $v){
            $message = $callback($k, $v, $this);
            $this->_processor->current_record++;
            if($message) $this->_processor->message = $message;
            $this->_processor->save();
        }

        $this->_processor->delete();

        die();
    }
}
