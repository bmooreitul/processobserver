<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessObserverBatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_observer_batches', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('message')->nullable();
            $table->bigInteger('total_records')->default(0);
            $table->bigInteger('current_record')->default(0);
            $table->bigInteger('pid')->nullable();
            $table->double('average_task_time', 12, 5)->default('0.01000');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_observer_batches');
    }
}
