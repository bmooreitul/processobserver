<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessObserverTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('process_observer_tasks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('process_observer_batch_id');
            $table->enum('status', ['pending','success','error'])->default('pending');       
            $table->string('message')->nullable();
            $table->binary('callback')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('process_observer_tasks');
    }
}
