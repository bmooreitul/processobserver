<?php

namespace Itul\ProcessObserver\Providers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Itul\ProcessObserver\ProcessObserver;

/**
 * Class ClientServiceProvider
 *
 * @package Itul\QuickBooks
 */
class ClientServiceProvider extends LaravelServiceProvider{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(){
        return [
            ProcessObserver::class,
        ];
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){
        $this->app->alias(ProcessObserver::class, 'ProcessObserver');
    }
}
